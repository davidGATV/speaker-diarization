import sys
sys.path.append("..")

from services.services import SpeechAnalysisService

if __name__ == '__main__':
    serv = SpeechAnalysisService()
    media = "serie_A"
    episode = 1
    serv.init_manager()
    serv.predict_speaker_diarization(media=media, episode=episode)
    output = serv.output
    print(output)