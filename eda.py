from sklearn.manifold import TSNE
import numpy as np
import os
import json
import matplotlib.pyplot as plt
import matplotlib.cm as cm


def read_json(filename):
    with open(filename) as json_file:
        data = json.load(json_file)
    return data

audio_output_dir = "E:\\David\\Datasets\\EasyTV\\serie\\Dataset\\audio_output"
output_filename_labels = "labels_encode.json"
speaker_names_dict = read_json(filename=os.path.join(audio_output_dir,
                                                     output_filename_labels))

total_speakers = len(speaker_names_dict.keys())
embedding_clusters = []
embedding_data = np.array([])
total_labels = []
dimension = 1000
for i in range(total_speakers):
    name = 'sp_centroid_emb_' + speaker_names_dict[str(i)] + '.npy'
    path = os.path.join("E:\\David\\Datasets\\EasyTV\\serie\\Dataset\\audio_embeddings",name)
    name_data = 'sp_emb_' + speaker_names_dict[str(i)] + '.npy'
    path_data = os.path.join("E:\\David\\Datasets\\EasyTV\\serie\\Dataset\\audio_embeddings",name_data)

    x_embd_centroid = np.load(path)
    x_embd = np.load(path_data)

    # get only 500 points
    if x_embd.shape[0] >= dimension:
        idx = np.random.randint(x_embd.shape[0], size=dimension)
        x_embd = x_embd[idx.tolist()]
    # ------------------------------
    total_labels += [speaker_names_dict[str(i)] for j in range(x_embd.shape[0])]
    print(x_embd.shape)

    if i ==0:
        embedding_data = x_embd
    else:
        embedding_data = np.vstack((embedding_data, x_embd))
    embedding_clusters.append(x_embd_centroid)

embedding_clusters = np.array(embedding_clusters)

tsne_model_en_2d = TSNE(perplexity=15, n_components=2, init='pca', n_iter=3500, random_state=32,verbose=1)
embeddings_en_2d_data = np.array(tsne_model_en_2d.fit_transform(embedding_data))
embeddings_en_2d = np.array(tsne_model_en_2d.fit_transform(embedding_clusters))


labels = list(speaker_names_dict.values())
filename = 'embeddings.png'
title = 'Audio Embeddings'
alpha = 0.7
colors = cm.tab20(np.linspace(0, 1, len(labels)))

total_colors = []
cc = list(speaker_names_dict.values())
for i, n in enumerate(total_labels):
    total_colors.append(colors[cc.index(n)])

# Create dataframe
import pandas as pd
import seaborn as sns

data_dct = {'embedding_1': embeddings_en_2d_data[:,0],
            'embedding_2': embeddings_en_2d_data[:,1],
            'colors': total_colors, 'labels':total_labels}

df = pd.DataFrame(data_dct)
sns.scatterplot(x="embedding_1", y="embedding_2", hue="labels",palette="Set2",s=100,
                data=data_dct)
plt.xlabel('TSNE embedding component 1')
plt.ylabel('TSNE embedding component 2')
plt.grid()
if filename:
    plt.savefig(filename, format='png', dpi=150, bbox_inches='tight')

plt.figure(figsize=(16, 9))
plt.scatter(embeddings_en_2d_data[:,0], embeddings_en_2d_data[:,1],
            alpha=alpha, c=total_colors, label=total_labels)
#plt.scatter(embeddings_en_2d[:,0], embeddings_en_2d[:,1], c=colors, alpha=alpha, marker="x", s=40)
plt.xlabel('TSNE embedding component 1')
plt.ylabel('TSNE embedding component 2')
plt.legend(total_labels)
plt.grid()
if filename:
    plt.savefig(filename, format='png', dpi=150, bbox_inches='tight')

