import json
from flask import Flask, request
from helper import config as cfg
from flask_cors import CORS
from services.services import SpeechAnalysisService

serv = SpeechAnalysisService()
app = Flask(__name__)
CORS(app)

# --------------------------------------------------------
# -------------- Voice Activity Detection ----------------
# --------------------------------------------------------

@app.route('/speech_analysis/vad', methods=['POST'])
def call_voice_activity_detection():
    data = request.get_json(force=True)
    if serv.speakerAn_manager is None:
        serv.init_manager()
    serv.execute_vad(data)
    output = serv.output
    return json.dumps(output)


@app.route('/speech_analysis/speaker_diarization_custom', methods=['POST'])
def call_speaker_diarization():
    data = request.get_json(force=True)
    if serv.speakerAn_manager is None:
        serv.init_manager()
    serv.execute_custom_speaker_diarization(data)
    output = serv.output
    return json.dumps(output)


@app.route('/speech_analysis/speaker_diarization/generate_dataset/<media>', methods=['GET'])
def call_generate_dataset(media):
    if serv.speakerAn_manager is None:
        serv.init_manager()
    serv.generate_new_dataset(media)
    output = serv.output
    return json.dumps(output)

# ------------------------------------------------------------------------------
# ---------------------- Speaker Diarization EasyTV ----------------------------
# ------------------------------------------------------------------------------


@app.route('/speech_analysis/speaker_diarization/train/<media>', methods=['GET'])
def call_speaker_diarization_train(media):
    # Audio Serie directory
    if serv.speakerAn_manager is None:
        serv.init_manager()
    serv.train_speaker_diarization_model(media)
    output = serv.output
    return json.dumps(output)


@app.route('/speech_analysis/speaker_diarization/embedding/<media>', methods=['GET'])
def call_speaker_diarization_embedding(media):
    if serv.speakerAn_manager is None:
        serv.init_manager()
    serv.generate_speaker_embeddings(media)
    output = serv.output
    return json.dumps(output)


@app.route('/speech_analysis/speaker_diarization/predict/<media>/<episode>', methods=['GET'])
def call_speaker_diarization_predict(media, episode):
    if serv.speakerAn_manager is None:
        serv.init_manager()
    serv.predict_speaker_diarization(media=media, episode=episode)
    output = serv.output
    return json.dumps(output)


if __name__ == '__main__':
    app.run(host=cfg.host, port=cfg.port, debug=False)