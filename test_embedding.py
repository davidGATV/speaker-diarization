import numpy as np
import glob
import os
from helper.utilities import read_json, unison_shuffled_copies
from sklearn.svm import SVC
import hdbscan
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report

embedding_dir = "E:\\David\\Datasets\\EasyTV\\serie_A\\Dataset\\data_output_normalize\\models_output\\embeddings\\"
all_emb_orig_class = np.load(os.path.join(embedding_dir, "Conv1D_emb_Claudia.npy"))
cov = np.cov(all_emb_orig_class.T)
input_shape = all_emb_orig_class.shape

f = 256
x_emb = np.array([])
y_emb = np.array([])
cont = 0
categories_path = read_json("E:\\David\\Datasets\\EasyTV\\serie_A\\Dataset\\data_output_normalize\\categories\\categories.json")
speakers = []
for i, file in enumerate(glob.glob("E:\\David\\Datasets\\EasyTV\\serie_A\\Dataset\\data_output_normalize\\models_output\\embeddings\\*.npy")):
    if 'centroid' not in file:
        print(cont)
        sp_name = file.split('_')[-1].split('.')[0]
        get_cat = int(list(categories_path.keys())[list(categories_path.values()).index(sp_name)])
        x = np.load(file)
        y = np.repeat(int(get_cat), x.shape[0])
        if cont == 0:
            x_emb = x
            y_emb = y
        else:
            x_emb = np.concatenate([x_emb, x], axis=0)
            y_emb = np.concatenate([y_emb, y], axis=0)
        cont += 1
xx, yy = unison_shuffled_copies(x_emb, y_emb)

val = 2
indx = np.where(yy==val)[0]

# Train per embedding

x_per_emb = xx[indx]
y_per_emb = yy[indx]

clusterer = hdbscan.HDBSCAN(min_cluster_size=5, prediction_data=True)
y_pred_tr_cluster = clusterer.fit_predict(x_per_emb)

# Remove values equal to -1
indx_r = np.where(y_pred_tr_cluster==val)
xx_r = x_per_emb[indx_r]
yy_r = y_per_emb[indx_r]
print("Removed: ", (y_per_emb.shape[0]- yy_r.shape[0]))

# Report Results
report_tr_cluster = classification_report(y_pred=y_pred_tr_cluster[indx_r].tolist(), y_true=yy_r)

# Predict
#y_pred_cluster = hdbscan.approximate_predict(clusterer=clusterer, points_to_predict=X_test)[0]
#report_pred_cluster = classification_report(y_pred=y_pred_cluster, y_true=y_test)

# Remove all the data points that were clustered as -1
REMOVE = True
if REMOVE:
    y_pred_all = clusterer.fit_predict(xx)
    indx = np.where(y_pred_all!=-1)
    xx = xx[indx]
    yy = yy[indx]
    X_train, X_test, y_train, y_test = train_test_split(xx, yy, test_size=0.33, random_state=42)

# -------------------------------------------------------------------------------------------------
# SVM
SVM = False
if SVM:
    X_train, X_test, y_train, y_test = train_test_split(xx, yy, test_size=0.33, random_state=42)
    clf = SVC(gamma='auto')
    # Train
    clf.fit(X_train, y_train)
    y_pred_tr_svm = clf.predict(X_train)
    report_tr_svm = classification_report(y_pred=y_pred_tr_svm, y_true=y_train)

    y_pred_svm = clf.predict(X_test)
    report_pred_svm = classification_report(y_pred=y_pred_svm, y_true=y_test)

# -------------------------------------------------------------------------------------------------

# Plot embeddings
from sklearn.manifold import TSNE, MDS
import pandas as pd
import seaborn as sns
sns.set()

tsne = TSNE(perplexity=30,learning_rate=1000, early_exaggeration=1)
tsne_embeddings = tsne.fit_transform(xx)
print(tsne_embeddings.shape)

data = {"tsne-1": tsne_embeddings[:, 0].tolist(),
        "tsne-2":tsne_embeddings[:, 1].tolist(),
        "speaker": [categories_path[str(i)] for i in yy.tolist()]}
df = pd.DataFrame(data)

# Plot Data
ax = sns.scatterplot(x="tsne-1", y="tsne-2", hue="speaker",
                     data=df, s=50)

# -------------------------------------------------------------------------------------------------
