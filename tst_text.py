import os
import numpy as np
import webrtcvad
from scipy.io import wavfile
import matplotlib.pyplot as plt
import librosa
from pydub import AudioSegment
from pydub.utils import mediainfo
import soundfile as sf

# Read file
os.environ['parent_dir'] = os.path.join("E:\\David", 'Datasets', 'speech_recognition')
train_audio_path = os.path.join(os.getenv('parent_dir'), 'train', 'audio')
audio_path = "harvard.wav" #os.path.join(train_audio_path, 'yes' , '0a7c2a8d_nohash_0.wav')


def preprocess_audio_file(audio_path):
    try:
        # 1) Convert to .wav
        if not audio_path.find(".wav"):
            if audio_path.find(".mp3"):
                sound = AudioSegment.from_mp3(audio_path)
                sound.export(audio_path.replace(".mp3", ".wav"), format="wav")
            elif audio_path.find(".ogg"):
                sound = AudioSegment.from_ogg(audio_path)
                sound.export(audio_path.replace(".ogg", ".wav"), format="wav")

            audio_path = audio_path.split('.')[0] + ".wav"

        # 2) Access to Media info
        info = mediainfo(audio_path)

        # Convert to Mono
        num_channels = int(info['channels'])
        sample_rate = int(info['sample_rate'])

        if num_channels != 1:
            sound = AudioSegment.from_wav(audio_path)
            sound = sound.set_channels(1)
            sound.export(audio_path, format="wav")

        # Resampling
        if sample_rate not in [8000, 16000, 32000, 48000]:
            new_sample_rate = 32000
            ori_samples, sample_rate = sf.read(audio_path)
            re_samples = librosa.resample(ori_samples , sample_rate, new_sample_rate)
            sf.write(audio_path, re_samples, new_sample_rate)

        # 3) Read data
        sample_rate, samples = wavfile.read(audio_path)

    except Exception as e:
        print(e)
        samples = None
        sample_rate = None
    return samples, sample_rate

samples, sample_rate = preprocess_audio_file(audio_path=audio_path)

vad = webrtcvad.Vad()

# set aggressiveness from 0 to 3
vad.set_mode(3)

import struct
raw_samples = struct.pack("%dh" % len(samples), *samples)

window_duration = 0.03 # duration in seconds

samples_per_window = int(window_duration * sample_rate + 0.5)

bytes_per_sample = 2

segments = []

total_samples = np.arange(0, len(samples), samples_per_window)

for i, start in enumerate(total_samples):

    print('{} / {}'.format(i+1, len(total_samples)))
    stop = min(start + samples_per_window, len(samples))
    try:
        is_speech = vad.is_speech(raw_samples[start * bytes_per_sample: stop * bytes_per_sample],
                                  sample_rate=sample_rate)
        # Convert samples to time (s)
        start_s = start / sample_rate
        stop_s = stop / sample_rate
        segments.append(dict(
            start=start,
            stop=stop,
            is_speech=is_speech))
    except Exception as e:
        print(e)
        continue

ymax = max(samples)
ymin = min(samples)
y_v = np.linspace(ymin* 1.1, ymax * 1.1, 10000)

def format_func(value, tick_number):
    return value/sample_rate



fig = plt.figure(figsize = (10,7))
ax = fig.gca()
plt.interactive(False)
ax.plot(samples)
# plot segment identified as speech
vertical_lines = []
temp_seg = {'start':-1, 'stop': -1, 'is_speech':False}
for i, segment in enumerate(segments):
    if segment['is_speech']:
        x_v_1 = segment['start'] * np.ones(len(y_v))
        x_v_2 = segment['stop'] * np.ones(len(y_v))
        ax.plot([ segment['start'], segment['stop'] - 1], [ymax * 1.1, ymax * 1.1], color = 'orange')
        ax.plot([segment['start'], segment['stop'] - 1], [ymin * 1.1, ymin * 1.1], color='orange')
        ax.plot(x_v_1,y_v, color='green', alpha=0.25)
        ax.plot(x_v_1, y_v, color='green', alpha=0.25)
    temp_seg = segment
ax.set_xlabel('Time (s)')
ax.set_ylabel('Amplitude')
ax.xaxis.set_major_formatter(plt.FuncFormatter(format_func))
ax.grid()

#speech_samples = np.concatenate([ samples[segment['start']:segment['stop']] for segment in segments if segment['is_speech']])






