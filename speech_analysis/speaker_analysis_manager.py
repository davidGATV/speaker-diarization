import numpy as np
import pandas as pd
import os
import speechpy
import hdbscan
from helper import config as cfg
from helper.utilities import (read_json, write_json, generate_json_format, prepare_directory,
                              string_to_int, normVec, unison_shuffled_copies, save_pipeline,
                              load_pipeline, concatenate_data)
from speech_analysis import voice_activity_detection as vad
from speech_analysis.audio_preprocessing import AudioPreprocessing
from speech_analysis.deep_speakerDir_net import SpeakerDirNet
from sklearn.utils import shuffle
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split, StratifiedKFold, cross_val_score
from sklearn.svm import SVC
from sklearn.metrics import classification_report
from tqdm import tqdm


class SpeakerAnalysisManager:
    def __init__(self):

        # Directories
        self.parent_dir = os.getenv('media_dir') # Parent directory
        self.service_parent_dir = cfg.service_parent_dir
        self.serie_dir = None # serie_A
        self.episode_dir = None
        self.dataset_dir = cfg.dataset_dir # Dataset
        self.audio_dir = cfg.audio_dir # audio
        self.data_output_dir = cfg.data_output_dir
        self.audio_output_dir = cfg.audio_output_dir # audio_output
        self.categories_dir = cfg.categories_dir # categories
        self.categories_filename_path = None
        self.models_output_dir = cfg.models_output_dir # models_output
        self.embedding_dir = cfg.embedding_dir # embeddings
        self.trained_models_dir = cfg.trained_models_dir # trained_models
        self.trained_models_history_dir = cfg.trained_models_history_dir # trained_models_history
        self.trained_models_results_dir = cfg.trained_models_results_dir # trained_models_results
        self.audio_path = None

        # DataFrame parameters
        self.target_col = cfg.response
        self.filename_col = cfg.filename_col
        self.target_col_enc = self.target_col + "_code"
        self.dataset_df = None
        self.categories_names = None
        self.categories_keys = None
        self.categories_data = None
        self.new_class_label = cfg.new_class_label

        self.categories_filename = cfg.categories_filename
        self.vad_service_name = cfg.vad_service_name
        # Audio Parameters
        self.spec_type = cfg.spec_type
        self.sr = cfg.sr
        self.n_mels = cfg.n_mels
        self.n_fft = cfg.n_fft
        self.hop_length = cfg.hop_length
        self.power = cfg.power
        self.fmax = cfg.fmax
        self.fmin = cfg.fmin
        self.duration = cfg.duration
        self.frame_length = cfg.frame_length
        self.data_augmentation = cfg.data_augmentation
        self.apply_strech = cfg.streech
        self.apply_pitch_mod = cfg.pitch
        self.apply_noise = cfg.noise
        self.normalize_spectrogram = cfg.normalize_spectrogram

        # Model Parameters
        self.split_test = cfg.split_test
        self.output_dim = -1
        self.output_act = cfg.output_act
        self.hidden_act = cfg.hidden_act
        self.embedding_layer = cfg.embedding_layer
        self.optimizer_name = cfg.optimizer_name
        self.batch_size = cfg.batch_size
        self.epochs = cfg.epochs
        self.feature_model_name = cfg.feature_model_name

        # Filenames
        self.model_name = cfg.model_name
        self.model_history_name = cfg.model_history_name
        self.weight_data_name = cfg.weight_data_name
        self.evaluation_results_name = cfg.evaluation_results_name
        self.embedding_name = cfg.embedding_name
        self.centroid_emb_name = cfg.centroid_emb_name

        # Model parameters
        self.metric = cfg.metric
        self.loss = cfg.loss
        self.k_fold = cfg.k_fold
        self.model = None
        self.raw_shape = None

        # Training/Validation data
        self.data = None
        self.data_train = None
        self.data_test = None
        self.x_train_val = None
        self.y_train_val = None
        self.x_train = None
        self.y_train = None
        self.x_val = None
        self.y_val = None

        self.total_dist = {}
        self.centroid_embs = np.array([])
        self.significance_level = cfg.significance_level
        self.vad = cfg.vad
        self.voice_segments = None

        # Custom objects
        self.ap = None
        self.speakerDirNet = None

        # Service Parameters
        self.service_name = cfg.sp_predict_service_name
        self.status = None
        self.output = {}
        self.non_smooth_output = {'start': [], 'end': [], 'speaker': []}
        self.smooth_output = []
        self.total_average = []
        self.total_sd = []
        self.total_min = []

    def generate_main_directories(self):
        try:
            if self.serie_dir is not None:
                # 1
                self.serie_dir = os.path.join(self.parent_dir, self.serie_dir)
                # 1.1
                self.dataset_dir = os.path.join(self.serie_dir, self.dataset_dir)
                # 1.2
                if self.episode_dir is not None:
                    self.episode_dir = os.path.join(self.serie_dir, self.episode_dir)
                # 2.1
                self.audio_dir = os.path.join(self.dataset_dir, self.audio_dir)
                # 2.2
                self.data_output_dir = os.path.join(self.dataset_dir, self.data_output_dir)
                # 2.2.1
                self.audio_output_dir = os.path.join(self.data_output_dir, self.audio_output_dir)
                # 2.2.2
                self.categories_dir = os.path.join(self.data_output_dir, self.categories_dir)
                self.categories_filename_path = os.path.join(self.categories_dir, self.categories_filename)
                # 2.2.3
                self.models_output_dir = os.path.join(self.data_output_dir, self.models_output_dir)
                # 2.2.3.1
                self.embedding_dir = os.path.join(self.models_output_dir, self.embedding_dir)
                # 2.2.3.2
                self.trained_models_dir = os.path.join(self.models_output_dir, self.trained_models_dir)
                # 2.2.3.3
                self.trained_models_history_dir = os.path.join(self.models_output_dir,
                                                               self.trained_models_history_dir)
                # 2.2.3.4
                self.trained_models_results_dir = os.path.join(self.models_output_dir,
                                                               self.trained_models_results_dir)

        except Exception as e:
            cfg.logger.error(e)
            self.status = cfg.error_msg + str(e)
        return self

    def make_main_directories(self):
        try:
            prepare_directory(self.audio_output_dir)
            prepare_directory(self.categories_dir)
            prepare_directory(self.models_output_dir)
            prepare_directory(self.embedding_dir)
            prepare_directory(self.trained_models_dir)
            prepare_directory(self.trained_models_history_dir)
            prepare_directory(self.trained_models_results_dir)
        except Exception as e:
            cfg.logger.error(e)
        return self

    def init_preprocessing(self):
        try:
            self.ap = AudioPreprocessing(spec_type=self.spec_type, sr=self.sr, n_mels=self.n_mels,
                                         n_fft=self.n_fft, hop_length=self.hop_length, power=self.power,fmin=self.fmin,
                                         fmax=self.fmax, frame_length=self.frame_length, response=self.target_col,
                                         filename_col=self.filename_col, data_augmentation=self.data_augmentation,
                                         apply_strech=self.apply_strech, apply_pitch_mod=self.apply_pitch_mod,
                                         apply_noise=self.apply_noise, audio_output_dir=self.audio_output_dir)
        except Exception as e:
            cfg.logger(e)
            self.status = cfg.error_msg + str(e)
        return self

    def init_speakerDirNet(self):
        try:
            self.speakerDirNet = SpeakerDirNet(input_shape=self.raw_shape, metric=self.metric,
                                               loss=self.loss, output_dim=self.output_dim,
                                               output_act=self.output_act, hidden_act=self.hidden_act,
                                               embedding_layer=self.embedding_layer,
                                               optimizer_name=self.optimizer_name, epochs=self.epochs,
                                               batch_size=self.batch_size, parent_dir=self.parent_dir,
                                               models_output_parent_dir=self.models_output_dir,
                                               embedding_dir=self.embedding_dir,
                                               feature_model_name=self.feature_model_name,
                                               model_trained_dir=self.trained_models_dir,
                                               model_eval_dir=self.trained_models_results_dir,
                                               model_trained_history_dir=self.trained_models_history_dir,
                                               model_name=self.model_name, model_history_name=self.model_history_name,
                                               weight_data_name=self.weight_data_name,
                                               evaluation_results_name=self.evaluation_results_name,
                                               embedding_filename=self.embedding_name,
                                               centroid_emb_filename=self.centroid_emb_name)
        except Exception as e:
            cfg.logger.error(e)
        return self

    def get_categories(self):
        try:
            # read data
            if self.categories_data is None:
                self.categories_data = read_json(json_path=self.categories_filename_path)
                self.categories_keys = list(self.categories_data.keys())
                self.categories_names = list(self.categories_data.values())
        except Exception as e:
            cfg.logger.error(e)
        return self

    def generate_train_test_from_dataframe(self):
        labels = []
        audio_name = []
        try:
            cfg.logger.info('Generating DataFrame for training and Testing')
            for folder in os.listdir(self.audio_dir):
                cfg.logger.info('Collecting data from %s', folder)
                # If the directory is a folder
                if os.path.isdir(os.path.join(self.audio_dir, folder)):
                    # Save Audio Path
                    audio_list = (os.listdir(os.path.join(self.audio_dir, folder)))
                    audio_path = [os.path.join(self.audio_dir, folder, f) for f in audio_list]
                    audio_name += audio_path
                    # Save Label
                    labels += [folder for l in range(len(audio_list))]
                    # Create DataFrame
            data = {self.filename_col: audio_name, self.target_col: labels}
            self.dataset_df = pd.DataFrame(data)
            self.dataset_df = shuffle(self.dataset_df).reset_index(drop=True)

            enc = LabelEncoder()
            enc.fit(self.dataset_df[[self.target_col]])
            self.dataset_df[self.target_col_enc] = enc.transform(self.dataset_df[[self.target_col]])

            cfg.logger.info('Separate into Train/test datasets ...')
            stratify_col = np.array(self.dataset_df[[self.target_col_enc]].values.tolist())
            data_train, data_test = train_test_split(self.dataset_df, test_size=self.split_test,
                                                     random_state=cfg.random_state, stratify=stratify_col)

            # Reset index
            self.data_train = data_train.reset_index(drop=True)
            self.data_test = data_test.reset_index(drop=True)

            # Save dictionary with keys/values
            self.categories_keys = [str(i) for i in enc.transform(enc.classes_)]
            self.categories_names = enc.classes_
            self.categories_data = dict(zip(self.categories_keys, self.categories_names))

            self.categories_dir = os.path.join(self.categories_dir, self.categories_filename)
            labels_json = generate_json_format(self.categories_data, output_str=False)
            write_json(json_data=labels_json, json_path=self.categories_dir)

        except Exception as e:
            cfg.logger.error(e)
            self.status = cfg.error_msg + str(e)
        return self

    def build_dataset(self, mode="train"):
        d = []
        self.status = cfg.success_msg
        try:
            # Train
            if mode == "train":
                self.data = self.data_train
            else:
                self.data = self.data_test

            cfg.logger.info("Creating x, y to %s ... \n", mode)
            with tqdm(total=self.data.shape[0]) as pbar:
                for index, row in self.data.iterrows():
                    pbar.update(1)
                    try:
                        # Retrieve Data
                        audio_file = row[self.filename_col]
                        target = row[self.target_col_enc]

                        # Load data
                        s = self.ap.load_audio_file(file_path=audio_file)

                        signal_preemphasized = speechpy.processing.preemphasis(s, cof=0.98)

                        # Remove initial silence from audio
                        s_n = self.ap.trim_audio_signal(x=signal_preemphasized)
                        done = True
                        offset = 0

                        # Overlapped window
                        while done:
                            done, frame = self.ap.analyze_audio_frame(s=s_n, offset=offset)
                            offset += int(self.frame_length/2)
                            frame_normalize = self.ap.normalize_audio_segment(frame=frame,
                                                                              frame_length=self.frame_length)
                            spectrogram = self.ap.extract_features(frame_normalize, normalize=self.normalize_spectrogram)

                            # Save Original Signal
                            if spectrogram is not None:
                                d.append((spectrogram, target))
                            if self.data_augmentation:
                                ss_augmented = self.ap.apply_data_augmentation(frames=frame_normalize,
                                                                               normalize=self.normalize_spectrogram)
                                if ss_augmented:
                                    for y_changed_s in ss_augmented:
                                            d.append((y_changed_s, target))

                            # Save Data
                            self.ap.save_data_locally(data=d, mode=mode)
                            d = []
                    except Exception as e:
                        cfg.logger.error(e)
                        continue
        except Exception as e:
            cfg.logger.error(e)
            self.status = cfg.error_msg + str(e)
        return self

    def train(self):
        try:
            self.status = cfg.success_msg
            # 1) Prepare Data
            self.load_data_train_val_test()
            # 2) Get categories and number of dimensions
            if self.categories_data is None:
                self.get_categories()
            self.output_dim = len(list(self.categories_data.keys()))

            # 3) Init Neural Network object
            if self.speakerDirNet is None:
                self.init_speakerDirNet()
                self.speakerDirNet.reset_tf_session()

            with self.speakerDirNet.session.as_default():
                with self.speakerDirNet.graph.as_default():
                    # Cross-validation
                    skf = StratifiedKFold(n_splits=self.k_fold, shuffle=True)
                    splits = skf.split(self.x_train_val, self.y_train_val)

                    for index, (train_indices, val_indices) in enumerate(splits):
                        cfg.logger.info("Training on fold %s/%s", index + 1, self.k_fold)

                        self.get_train_val_data_from_indexes(train_indices=train_indices,
                                                             val_indices=val_indices)
                        unique_cat = string_to_int(self.categories_keys)

                        # Re-start Model and Parameters to re-train the model
                        self.speakerDirNet.extract_conv2D_params()
                        self.speakerDirNet.model = None

                        # Build Model
                        self.speakerDirNet.build_feature_extraction_classifier()

                        # Plot model
                        # self.speakerDirNet.plot_speakerDirNet()

                        # Compile Model
                        self.speakerDirNet.compile_speakerDirNet()

                        self.speakerDirNet.train_speakerDirNet(x_train=self.x_train, y_train=self.y_train,
                                                               x_val=None, y_val=None,
                                                               unique_categories=unique_cat,
                                                               n_classes=self.output_dim)
                        # Evaluate model
                        self.speakerDirNet.evaluate_model(model=self.speakerDirNet.model, x_test=self.x_val,
                                                          y_test=self.y_val)
                        cfg.logger.info("Model Evaluation at k-fold(%s): %s (+/- %s)", index+1,
                                        self.speakerDirNet.scores,
                                        self.speakerDirNet.scores)

                        k = index + 1
                        # Save History
                        self.speakerDirNet.save_history_speakerDirNet(k_fold=k)
                        # Save Metrics
                        self.speakerDirNet.save_evaluation_metrics(k_fold=k)

        except Exception as e:
            cfg.logger.error(e)
            self.status = cfg.error_msg + str(e)
        return self

    def load_data_train_val_test(self):
        try:
            self.x_train_val, self.y_train_val = self.ap.load_dataset(mode="train")
            self.x_test, self.y_test = self.ap.load_dataset(mode='test')

            # Adequate Shapes
            if self.feature_model_name == "Conv2D":
                self.raw_shape = self.x_train_val.shape[1:] + (1,)
                self.x_train_val = self.x_train_val[:, :, :, np.newaxis]
                self.x_test = self.x_test[:, :, :, np.newaxis]
            else:
                self.raw_shape = self.x_train_val.shape[1:]

            # Check y_train
            self.y_train_val = np.array(self.y_train_val.flatten().tolist())
            self.y_test = np.array(self.y_test.flatten().tolist())

            # Shuffle Data
            self.x_train_val, self.y_train_val = unison_shuffled_copies(self.x_train_val,
                                                                        self.y_train_val)
            self.x_test, self.y_test = unison_shuffled_copies(self.x_test, self.y_test)

            # Concatenate all data
            self.x_train_val, self.y_train_val = concatenate_data(x_train=self.x_train_val,
                                                                  x_test=self.x_test,
                                                                  y_train=self.y_train_val,
                                                                  y_test=self.y_test)

        except Exception as e:
            cfg.logger.error(e)
            self.status = cfg.error_msg + str(e)
        return self

    def get_train_val_data_from_indexes(self, train_indices, val_indices):
        try:
            if not type(self.x_train_val) == np.ndarray:
                # Generate batches from indices
                self.x_train, self.x_val = np.array(self.x_train_val.iloc[train_indices]).round(3), \
                                 np.array(self.x_train_val.iloc[val_indices]).round(3)
                self.y_train, self.y_val = np.array(self.y_train_val.iloc[train_indices]).round(3), \
                                 np.array(self.y_train_val.iloc[val_indices]).round(3)
            else:
                self.x_train, self.x_val = self.x_train_val[train_indices].round(3), self.x_train_val[
                    val_indices].round(3)
                self.y_train, self.y_val = self.y_train_val[train_indices].round(3), self.y_train_val[
                    val_indices].round(3)


        except Exception as e:
            cfg.logger.error(e)
            self.status = cfg.error_msg + str(e)
        return self

    def generate_embeddings(self, train_svm=True):
        try:
            self.status = cfg.success_msg

            # Load Data
            self.load_data_train_val_test()

            # Prepare object
            if self.speakerDirNet is None:
                self.init_speakerDirNet()
            if self.categories_data is None:
                self.get_categories()
            if self.speakerDirNet.session is None:
                self.speakerDirNet.reset_tf_session()

            # Concatenate data
            x,y = concatenate_data(x_train=self.x_train_val, x_test=self.x_test,
                                   y_train=self.y_train_val, y_test=self.y_test)
            all_embds = np.array([])
            all_cat_embds = np.array([])
            # Get all the indexes associated to each category
            total_cat = string_to_int(self.categories_keys)
            ind = {cat: [i for i, value in enumerate(y) if value == cat] for cat in total_cat}

            # Init the clustering HDBScan
            clusterer = hdbscan.HDBSCAN(min_cluster_size=5, prediction_data=True)
            cfg.logger.info("Generating embeddings ...\n")
            with self.speakerDirNet.session.as_default():
                with self.speakerDirNet.graph.as_default():
                    if self.model is None:
                        self.speakerDirNet.load_embedding_model(k_fold=5)
                        self.model = self.speakerDirNet.encoder
                    with tqdm(total=len(total_cat)) as pbar:
                        for i, cat in enumerate(total_cat):
                            pbar.update(1)
                            x_s = x[ind[cat]]
                            # Predict
                            x_emb = self.model.predict(x_s)
                            # Normalize vector
                            x_emb = np.array([normVec(i) for i in x_emb])
                            original_shape = x_emb.shape

                            # Remove the outlier embedding from the distribution
                            y_pred_emb_cluster = clusterer.fit_predict(x_emb)
                            indx_r = np.where(y_pred_emb_cluster != -1)[0]

                            # Remove only in case the clustering analysis detect one class
                            if (original_shape[0]- len(indx_r)) <= int((original_shape[0]/4)):
                                x_emb = x_emb[indx_r]
                            cleaned_shape = x_emb.shape
                            cfg.logger.warn("Number of embeddings removed: %s", (original_shape[0] - cleaned_shape[0]))

                            # Get centroid embedding
                            x_cent_emb = np.mean(x_emb, axis=0)
                            # Get Category name
                            cat_name = self.categories_data[str(cat)]
                            # Save embedding
                            self.speakerDirNet.save_embeddings_into_file(x_emb, x_cent_emb, cat_name)

                            # Append embedding
                            yy = np.repeat(i, x_emb.shape[0])
                            if i == 0:
                                all_embds = x_emb
                                all_cat_embds = yy
                            else:
                                all_embds = np.concatenate([all_embds, x_emb], axis=0)
                                all_cat_embds = np.concatenate([all_cat_embds, yy], axis=0)

            if train_svm:
                cfg.logger.info("Training SVM using the embeddings ... ")
                # Split Data
                X_train, X_test, y_train, y_test = train_test_split(all_embds, all_cat_embds,
                                                                    test_size=0.30, random_state=42)
                clf = SVC(C=0.5, gamma='auto')

                model_pipeline = Pipeline([("scaler", StandardScaler()),
                                           ("model", clf)])
                # Train using cross-validation
                scores = cross_val_score(model_pipeline,X_train, y_train, cv=5)
                cfg.logger.info("Accuracy: %s (+/- %s)" ,scores.mean(), scores.std() * 2)

                model_pipeline.fit(X_train, y_train)

                y_pred = model_pipeline.predict(X_test)
                print(classification_report(y_pred=y_pred, y_true=y_test))

                # Save
                pipeline_dir = "pipeline.pkl"
                save_pipeline(pipeline=model_pipeline, pipeline_dir=pipeline_dir)

        except Exception as e:
            cfg.logger.error(e)
            self.status = cfg.error_msg + str(e)
        return self

    def predict_speaker(self, audio_file):
        try:
            self.status = cfg.success_msg
            if self.audio_path is None:
                self.audio_path = os.path.join(self.episode_dir, audio_file)

            # Prepare data
            if self.categories_data is None:
                self.get_categories()

            # Load audio signal
            s = self.ap.load_audio_file(file_path=self.audio_path)
            # Use the Voice Activity detector
            if s is not None:
                # Voice Activity Detection
                self.execute_voice_activity_detector(filename=self.audio_path)
                # Extract statistics from embeddings
                self.compute_embeding_distance_statistics()

                # Load model
                if self.speakerDirNet is None:
                    self.init_speakerDirNet()
                    self.speakerDirNet.reset_tf_session()

                with self.speakerDirNet.session.as_default():
                    with self.speakerDirNet.graph.as_default():
                        # Load Model
                        if self.model is None:
                            self.speakerDirNet.load_embedding_model(k_fold=1)
                            self.model = self.speakerDirNet.encoder

                        # Main loop over each segment
                        with tqdm(total=len(self.voice_segments)) as pbar:
                            for ii, v_s in enumerate(self.voice_segments):
                                pbar.update(1)
                                # Convert seconds to frames
                                s_init = int(self.sr * v_s['start'])
                                s_end = int(self.sr * v_s['end'])
                                # Get audio subframe
                                audio_segment = s[s_init: s_end]

                                # Preemphasized signal
                                audio_segment = speechpy.processing.preemphasis(audio_segment, cof=0.98)
                                # Remove initial silence from audio
                                audio_segment = self.ap.trim_audio_signal(x=audio_segment)

                                done = True
                                offset = 0
                                # Overlapped window
                                while done:
                                    done, frame = self.ap.analyze_audio_frame(s=audio_segment, offset=offset)
                                    offset += int(self.frame_length / 2)
                                    frame_normalize = self.ap.normalize_audio_segment(frame=frame,
                                                                                      frame_length=self.frame_length)
                                    spectrogram = self.ap.extract_features(frame_normalize,
                                                                           normalize=self.normalize_spectrogram)

                                    # Adequate signal
                                    if self.feature_model_name == "Conv2D" and len(spectrogram.shape)<4:
                                        ss = spectrogram[:, :, :, np.newaxis]
                                    else:
                                        ss = spectrogram
                                    embd_pred = self.model.predict(ss)

                                    # Get speaker from embedding
                                    y_pred = self.get_speaker_from_embedding(embd_pred)

                                    # Get the speaker name from file
                                    self.non_smooth_output['speaker'].append(y_pred)
                                    self.non_smooth_output['start'].append(s_init/self.sr)
                                    self.non_smooth_output['end'].append(s_end/self.sr)
            # Generate smooth output
            self.generate_smooth_output()
        except Exception as e:
            cfg.logger.error(e)
            self.status = cfg.error_msg + str(e)
        return self

    def execute_voice_activity_detector(self, filename):
        try:
            v = vad.VoiceActivityDetection(parent_dir=self.service_parent_dir,audiofile=filename, save=False, outputFile=None,
                                           plot_regions=False)
            if self.vad == "inaVAD":
                # Voice Activity Detection
                v._run_inaVAD()
                vad_output = v.output[self.vad_service_name]
                # Analyse only those segments that contains voice
                self.voice_segments = [i for i in vad_output if i['event'] == 'Female' or i['event'] == 'Male']
            elif self.vad == "energyVAD":
                v._run_energyVAD()
                vad_output = v.output[self.vad_service_name]
                self.voice_segments = [i for i in vad_output if i['event'] == 'speech']
            elif self.vad == "webrtcVAD":
                v._run_webrtcVAD()
                vad_output = v.output[self.vad_service_name]
                self.voice_segments = [i for i in vad_output if i['event'] == 'speech']
            else:
                cfg.logger.warning("VAD method not valid")
        except Exception as e:
            cfg.logger.error(e)
            self.status = cfg.error_msg + str(e)
        return self

    def generate_smooth_output(self):
        try:
            new_speaker = -1
            add_speaker = True
            sp_index = 0
            for i, speaker in enumerate(self.non_smooth_output['speaker']):
                if i > 0:
                    # Continue the same speaker
                    if new_speaker == speaker:
                        self.smooth_output[sp_index]['end'] = self.non_smooth_output['end'][i]
                        add_speaker = False
                    else:
                        add_speaker = True

                # Add speaker
                if add_speaker:
                    new_speaker = speaker
                    self.smooth_output.append({'speaker': new_speaker,
                                               'start': np.round(self.non_smooth_output['start'][i],2),
                                               'end': np.round(self.non_smooth_output['end'][i],2)})

                    sp_index = len(self.smooth_output) - 1

        except Exception as e:
            cfg.logger.error(e)
            self.status = cfg.error_msg + str(e)
        return self

    def compute_embeding_distance_statistics(self):
        try:
            total_average = []
            total_sd = []
            total_min = []
            centroid_embs = np.array([])
            first_in = True

            with tqdm(total=len(self.categories_data)) as pbar:
                for k,v in self.categories_data.items():
                    pbar.update(1)
                    filename_emb = os.path.join(self.embedding_dir, self.embedding_name + str(v) + '.npy')
                    filename_cent = os.path.join(self.embedding_dir, self.centroid_emb_name + str(v) + '.npy')
                    embds = np.load(file=filename_emb)
                    c_emb = np.load(file=filename_cent).reshape((1, embds.shape[1]))

                    # Compute distance between the embeddings and its centroid
                    embds_ssm = np.concatenate([embds, c_emb], axis=0)
                    sim = cosine_similarity(embds_ssm)

                    # Remove diagonal and compute statistics
                    total_sd.append(np.std(sim[-1, :-1]))
                    total_average.append(np.mean(sim[-1, :-1]))
                    total_min.append(np.min(sim[-1, :-1]))
                    if first_in:
                        centroid_embs = c_emb
                        first_in = False
                    else:
                        centroid_embs = np.concatenate([centroid_embs, c_emb], axis=0)
            # Save data in local variables
            self.centroid_embs = centroid_embs
            self.total_sd = total_sd
            self.total_average = total_average
            self.total_min = total_min
        except Exception as e:
            cfg.logger.error(e)
            self.status = cfg.error_msg + str(e)
        return self

    def get_speaker_from_embedding(self, embd_pred):
        try:

            model_pipeline = load_pipeline(pipeline_dir="pipeline.pkl")
            sp_index = model_pipeline.predict(embd_pred).tolist()[0]
            predicted_speaker = self.categories_data[str(sp_index)]

            """centroid_embs = np.concatenate([self.centroid_embs, embd_pred], axis=0)

            # Compute similarity
            cos_sim = cosine_similarity(centroid_embs)

            # Get Maximum for the lat row removing diagonal (compute close distance)
            max_index = int(np.argmax(cos_sim[-1, :-1]))
            max_value = np.max(cos_sim[-1, :-1])

            # Threshold: Check maximum
            if max_value < self.total_min[max_index]:
                predicted_speaker = self.new_class_label
            else:
                predicted_speaker = self.categories_data[str(max_index)]"""
        except Exception as e:
            cfg.logger.error(e)
            self.status = cfg.error_msg + str(e)
            predicted_speaker = None
        return predicted_speaker