import os
import re
import json
import pandas as pd
import numpy as np
import scipy as sp
from scipy.stats import chi2
from helper import config as cfg
from sklearn.preprocessing import StandardScaler
from sklearn.externals import joblib


def read_csv_file(filename):
    try:
        data = pd.read_csv(filename)
    except Exception as e:
        cfg.logger.error(e)
        data = None
    return data

def build_output(name, task, status, data=None):
    output = {'Service': name, 'Task': task, 'Status': status}
    if data is not None:
        output.update({'Output': data})
    return output

def prepare_directory(dir_path):
    try:
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)
    except Exception as e:
        cfg.logger.error(e)
    return

def read_json(json_path):
    try:
        with open(json_path, 'r', encoding="utf8") as json_file:
            output = json.load(json_file)
    except Exception as e:
        cfg.logger.error(e)
        output = None
    return output

def read_architecture(json_path):
    try:
        with open(json_path) as json_file:
            output = json_file.read()
    except Exception as e:
        cfg.logger.error(e)
        output = None
    return output

def generate_json_format(input, output_str=True):
    output = None
    try:
        if output_str:
            output = json.dumps(str(input))
        else:
            output = json.dumps(input)
    except Exception as e:
        cfg.logger.error(e)
    return output


def write_json(json_data, json_path):
    try:
        with open(json_path, "w", encoding="utf8") as json_file:
            json_file.write(json_data)
    except Exception as e:
        cfg.logger.error(e)
    return


def standar_scale(x):
    try:
        scaler = StandardScaler()
        x_t = scaler.fit_transform(X=x)
    except Exception as e:
        cfg.logger.error(e)
        x_t = None
    return x_t


def normVec(vec):
    return(vec/np.linalg.norm(vec))


def write_numpy_file(numpy_filename, data):
    try:
        np.save(numpy_filename, data)
    except Exception as e:
        cfg.logger.error(e)
    return

def read_numpy_file(numpy_filename):
    try:
        data = np.load(file=numpy_filename)
    except Exception as e:
        cfg.logger.error(e)
        data = None
    return data


def mahalanobis(x=None, data=None, cov=None):
    mah_distance = None
    try:
        """Compute the Mahalanobis Distance between each row of x and the data
        x    : vector or matrix of data with, say, p columns.
        data : ndarray of the distribution from which Mahalanobis distance of each observation of x is to be computed.
        cov  : covariance matrix (p x p) of the distribution. If None, will be computed from data.
        """
        x_minus_mu = x - np.mean(data)
        if not cov:
            cov = np.cov(data.T)
        inv_covmat = sp.linalg.inv(cov)
        left_term = np.dot(x_minus_mu, inv_covmat)
        mahal = np.dot(left_term, x_minus_mu.T)
        mah_distance = mahal.diagonal()
    except Exception as e:
        cfg.logger.error(e)
    return mah_distance


def get_chi2_critical_value(significance_level, degrees_freedom):
    critical_value = None
    try:
        critical_value = chi2.ppf((1 - significance_level), df=(degrees_freedom - 1))
    except Exception as e:
        cfg.logger.error(e)
    return critical_value


def get_pvalue_chi2(dist, degrees_freedom):
    pvalues = None
    try:
        pvalues = 1 - chi2.cdf(dist, (degrees_freedom - 1))
    except Exception as e:
        cfg.logger.error(e)
    return pvalues

def collect_batch(batch_size, x, y, n_classes):
    pairs = [np.zeros((batch_size, x.shape[1])) for i in range(2)]
    targets = np.zeros((batch_size,))
    try:
        # DataFrame
        cols = ['d_' + str(i) for i in range(x.shape[1])]
        df = pd.DataFrame(x, columns=cols)
        df['target'] = [i[0] for i in y.tolist()]

        n_examples, n_d = x.shape

        # randomly sample several classes to use in the batch
        categories = np.random.choice(n_classes,size=(batch_size,), replace=False)

        # Make one half of it '1's, so 2nd half of batch has same class
        targets[batch_size // 2:] = 1

        for i in range(batch_size):
            category = categories[i]
            df_cat = df[df['target'] == category]
            idx_1 = np.random.randint(0, len(df_cat))
            df2 = df_cat.iloc[idx_1].drop(['target'],axis=0)
            pairs[0][i] = np.array(df2).reshape(n_d, )

            idx_2 = np.random.randint(0, len(df_cat))

            # pick images of same class for 1st half, different for 2nd
            if i >= batch_size // 2:
                category_2 = category
            else:
                # add a random number to the category modulo n classes to ensure 2nd image has a different category
                category_2 = (category + np.random.randint(1, n_classes)) % n_classes
                df_cat = df[df['target'] == category_2]
                idx_1 = np.random.randint(0, len(df_cat))
                df2 = df_cat.iloc[idx_1].drop(['target'], axis=0)

            pairs[1][i] = np.array(df2).reshape(n_d, )

    except Exception as e:
        cfg.logger.error(e)
    return pairs, targets


def get_random_batch(in_groups, batch_halfsize=8):
    out_img_a, out_img_b, out_score = [], [], []
    all_groups = list(range(len(in_groups)))

    for match_group in [True, False]:
        group_idx = np.random.choice(all_groups, size=batch_halfsize)
        out_img_a += [in_groups[c_idx][np.random.choice(range(in_groups[c_idx].shape[0]))] for c_idx in group_idx]
        if match_group:
            b_group_idx = group_idx
            out_score += [1] * batch_halfsize
        else:
            # anything but the same group
            non_group_idx = [np.random.choice([i for i in all_groups if i != c_idx]) for c_idx in group_idx]
            b_group_idx = non_group_idx
            out_score += [0] * batch_halfsize

        out_img_b += [in_groups[c_idx][np.random.choice(range(in_groups[c_idx].shape[0]))] for c_idx in b_group_idx]

    # Add new dimension
    pairs_1 = np.stack(out_img_a, 0)[:,:,:, np.newaxis]
    pairs_2 = np.stack(out_img_b, 0)[:,:,:, np.newaxis]
    score = np.stack(out_score, 0)

    # Shuffle data
    p = np.random.permutation(len(pairs_1))
    pairs_1 = pairs_1[p]
    pairs_2 = pairs_2[p]
    score = score[p]
    return pairs_1, pairs_2, score


def string_to_int(str_data):
    try:
        int_data = [int(i) for i in str_data]
    except Exception as e:
        cfg.logger.error(e)
        int_data = None
    return int_data


def get_nearest_even_number(x):
    try:
        x_n = round(x)
        # is answer now odd
        if x_n % 2:
            x_n -= 1
    except Exception as e:
        cfg.logger.error(e)
        x_n = x
    return x_n


def get_nearest_2_power_number(x):
    try:
        for i in range(1, 14):
            if 2 ** i >= x:
                x = 2 ** i
                break
    except Exception as e:
        cfg.logger.error(e)
    return x

def unison_shuffled_copies(a, b):
    try:
        p = np.random.permutation(len(a))
        a_n = a[p]
        b_n = b[p]
    except Exception as e:
        cfg.logger.error(e)
        a_n = a
        b_n = b
    return a_n, b_n


def update_path_name_by_string(full_path, index=os.sep, substr='_', additional_str=None, extension=None):
    updated_full_path = None
    try:
        # Get all the directories
        get_directories = full_path.split(index)
        # Retrieve last directory
        get_last_directory = get_directories[-1].split('.')[0]
        # Check if the last part of the directory contains subtr_kfold
        if re.findall("[_0-9]", get_last_directory.split(substr)[-1]):
            get_last_directory = substr.join(get_last_directory.split(substr)[:-1])

        # Join all the directories
        updated_full_path = os.path.join(index.join(get_directories[:-1]), get_last_directory)

        # Add additional string
        if additional_str is not None and isinstance(additional_str, str):
            updated_full_path += substr + additional_str
        # Add extension
        if extension is not None and isinstance(extension, str):
            updated_full_path += extension

    except Exception as e:
        cfg.logger.error(e)
    return updated_full_path


def save_pipeline(pipeline, pipeline_dir):
    try:
        with open(pipeline_dir, 'wb') as fid:
            joblib.dump(pipeline, fid)
    except Exception as e:
        cfg.logger.error(e)
    return


def load_pipeline(pipeline_dir):
    try:
        with open(pipeline_dir, 'rb') as fid:
            pipeline = joblib.load(fid)
    except Exception as e:
        cfg.logger.error(e)
        pipeline = None
    return pipeline


def concatenate_data(x_train, x_test,y_train, y_test):
    x, y = None, None
    try:
        x = np.concatenate([x_train, x_test], axis=0)
        y = np.concatenate([y_train, y_test], axis=0)
    except Exception as e:
        cfg.logger.error(e)
    return x,y