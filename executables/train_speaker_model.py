import sys
sys.path.append("..")

from services.services import SpeechAnalysisService


if __name__ == '__main__':
    serv = SpeechAnalysisService()
    media = "serie_A"
    serv.init_manager()
    serv.train_speaker_diarization_model(media)
    output = serv.output
    print(output)