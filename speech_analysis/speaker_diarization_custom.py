from helper import config as cfg
from pydub import AudioSegment
from pyAudioAnalysis.pyAudioAnalysis import audioSegmentation as aS
import os
import json
# -----------------------
# TODO
# 1) Speech Detection VAD
# 2) Speech Segmentation
# 3) Embedding Extraction
# 4) Clustering


class speakerDiarization():
    def __init__(self, audiofile, outputFile, n_speakers, mt_size, mt_step, st_win, lda_dim, plot_res=False,
                 save=True):
        self.audiofile = audiofile
        self.raw_audio = None
        self.n_speakers = n_speakers
        self.mt_size = mt_size
        self.mt_step = mt_step
        self.st_win = st_win
        self.lda_dim = lda_dim
        self.plot_res = plot_res
        self.output_dir = 'output'
        self.outputFile = os.path.join(self.output_dir, outputFile)
        self.speaker_diar = None
        self.output = None
        self.save = save
        self._convert_to_wav()

    def _convert_to_wav(self):
        try:
            wav_format = '.wav'
            self.raw_audio = AudioSegment.from_mp3(self.audiofile)
            self.audiofile = self.audiofile.split('.')[0] + wav_format
            self.raw_audio.export(self.audiofile.replace('mp3', 'wav'), format="wav")
        except Exception as e:
            cfg.logger.error(e)
        return self

    def _run(self):
        try:
            cfg.logger.info('Analyzing audiofile %s via Speaker Diarization process ...', self.audiofile)
            self.speaker_diar = aS.speakerDiarization(filename=self.audiofile,
                                                      n_speakers=self.n_speakers,
                                                      mt_size=self.mt_size,
                                                      mt_step=self.mt_step,
                                                      st_win=self.st_win,
                                                      lda_dim=self.lda_dim,
                                                      plot_res=self.plot_res)
            self.output = self.speaker_diar
            if self.save:
                self._save_output(data=self.output)
        except Exception as e:
            cfg.logger.error(e)
        return self

    def _save_output(self, data):
        try:
            cfg.logger.info('Saving Speaker Diarization Results at %s ', self.outputFile)

            if not os.path.exists(self.output_dir):
                os.makedirs(self.output_dir)

            with open(self.outputFile, 'w') as fp:
                json.dump(data, fp)

        except Exception as e:
            cfg.logger.error(e)
        return self