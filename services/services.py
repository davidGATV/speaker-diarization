import ast
import os
import glob
from helper.utilities import build_output
from speech_analysis import voice_activity_detection as vad
from speech_analysis import speaker_diarization_custom as sd
from speech_analysis.speaker_analysis_manager import SpeakerAnalysisManager
from helper import config as cfg

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


class SpeechAnalysisService:
    def __init__(self):
        self.data = None
        self.parent_dir = cfg.service_parent_dir
        self.service_name = cfg.main_service_name
        self.voice_activiy_detector = None
        self.speaker_diar_custom = None
        self.speakerAn_manager = None
        self.output = {}

    def init_manager(self):
        try:
            self.speakerAn_manager = SpeakerAnalysisManager()
        except Exception as e:
            cfg.logger.error(e)
        return self

    def set_up_manager(self):
        try:
            self.speakerAn_manager.generate_main_directories()
            self.speakerAn_manager.make_main_directories()
            self.speakerAn_manager.init_preprocessing()
        except Exception as e:
            cfg.logger.error(e)
        return self

    def execute_vad(self, data):
        try:
            self.data = data
            self.service_name = cfg.vad_service_name
            audioFile = self.data['audioFile']
            save = ast.literal_eval(self.data['save_output_locally'])
            model = self.data['model']
            outputFile = "output_" + str(model) + ".json"
            plot_regions = ast.literal_eval(self.data['plot_regions'])

            self.voice_activiy_detector = vad.VoiceActivityDetection(parent_dir=self.parent_dir,
                                                                     audiofile=audioFile, save=save,
                                                                     outputFile=outputFile,
                                                                     plot_regions=plot_regions)
            # Apply Voice Activity Detector
            if model == 'inaVAD':
                self.voice_activiy_detector._run_inaVAD()
            elif model == 'energyVAD':
                self.voice_activiy_detector._run_energyVAD()
            elif model == 'webrtcVAD':
                self.voice_activiy_detector._run_webrtcVAD()

            # Check for errors
            if self.voice_activiy_detector.output['Error'] == "False":
                self.output = self.voice_activiy_detector.output
                cfg.logger.info('The VAD service ended with 0 errors!. \n Try again with a new audio sample')
            else:
                self.output = {'Service': self.service_name, 'Error': self.voice_activiy_detector.output['Error']}
        except Exception as e:
            cfg.logger.error(e)
            self.output = {'Service': self.service_name, 'Error': str(e)}
        return self

    def execute_custom_speaker_diarization(self, data):
        try:
            self.data = data
            self.service_name = cfg.sp_custom_service_name
            audioFile = self.data['audioFile']
            outputFile = self.data['outputFile']
            n_speakers = self.data['n_speakers']
            mt_size = self.data['mt_size']
            mt_step = self.data['mt_step']
            st_win = self.data['st_win']
            lda_dim = self.data['lda_dim']
            plot_res = ast.literal_eval((self.data['show_plot']))
            save = ast.literal_eval(self.data['save_output_locally'])

            self.speaker_diar_custom = sd.speakerDiarization(audiofile=audioFile, outputFile=outputFile,
                                                             n_speakers=n_speakers, mt_size=mt_size,
                                                             mt_step=mt_step, st_win=st_win, lda_dim=lda_dim,
                                                             plot_res=plot_res, save=save)
            self.speaker_diar_custom._run()
            self.output = self.speaker_diar_custom.output
            if self.speaker_diar_custom.output is not None:
                cfg.logger.info('The ' + self.service_name + " service" + "ended with 0 errors!. \n Try again with a new audio sample")
        except Exception as e:
            cfg.logger.error(e)
            self.output = {'Service': self.service_name, 'Error': str(e)}
        return self

    def generate_new_dataset(self, media):
        task = cfg.generate_dataset_service_name
        try:
            self.speakerAn_manager.serie_dir = media
            self.set_up_manager()
            self.speakerAn_manager.generate_train_test_from_dataframe()
            # Train data
            self.speakerAn_manager.build_dataset(mode="train")
            # Test data
            self.speakerAn_manager.build_dataset(mode="test")

            # Generate output
            self.output = build_output(name=self.service_name, task=task,
                                       status=self.speakerAn_manager.status)
        except Exception as e:
            cfg.logger.error(e)
            status = 'An error occurred when generating the dataset. Error information: ' + str(e)
            self.output = build_output(name=self.service_name, task=task,
                                       status=status)
        return self

    def train_speaker_diarization_model(self, media):
        task = cfg.sp_training_service_name
        try:
            self.speakerAn_manager.serie_dir = media
            self.set_up_manager()

            self.speakerAn_manager.train()

            # Generate output
            self.output = build_output(name=self.service_name, task=task,
                                       status=self.speakerAn_manager.status)
        except Exception as e:
            cfg.logger.error(e)
            status = 'An error occurred when training the model. Error information: ' + str(e)
            self.output = build_output(name=self.service_name, task=task,
                                       status=status)
        return self

    def generate_speaker_embeddings(self, media):
        task = cfg.sp_embedding_service_name
        try:
            self.speakerAn_manager.serie_dir = media
            self.set_up_manager()

            # Calculate Embeddings
            self.speakerAn_manager.generate_embeddings()

            # Generate output
            self.output = build_output(name=self.service_name, task=task,
                                       status=self.speakerAn_manager.status)
        except Exception as e:
            cfg.logger.error(e)
            status = 'An error occurred when generating the embeddings. Error information: ' + str(e)
            self.output = build_output(name=self.service_name, task=task,
                                       status=status)
        return self

    def predict_speaker_diarization(self, media, episode):
        task = cfg.sp_predict_service_name
        try:
            self.speakerAn_manager.serie_dir = media
            self.speakerAn_manager.episode_dir = str(episode)
            self.set_up_manager()

            # Get audio_file
            full_path = os.path.join(self.speakerAn_manager.episode_dir, '*.wav')
            wav_file = glob.glob(full_path, recursive=True)[0].split(os.sep)[-1]
            self.speakerAn_manager.predict_speaker(audio_file=wav_file)
            data = self.speakerAn_manager.smooth_output
            self.output = build_output(name=self.service_name, task=task,
                                       status=self.speakerAn_manager.status, data=data)
        except Exception as e:
            cfg.logger.error(e)
            status = "An error occurred when analyzing episode: " + str(episode) + ". Error information: " + str(e)
            self.output = build_output(name=self.service_name, task=task,
                                       status=status, data=[])
        return self